const squares = document.querySelectorAll('.square');
const mole = document.querySelectorAll('.mole');
const timeLeft = document.getElementById("time-left");
const score = document.getElementById("score");

let result = 0;
let hitPosition;
let timerId = null;
let currentTime = 60;

function randomSquare() {
    squares.forEach((square) => {
        square.classList.remove('mole');
    })
    //for choosing random square to put mole
    let randomSquare = squares[Math.floor(Math.random() * 9)];
    randomSquare.classList.add('mole');
    hitPosition = randomSquare.id;
}

squares.forEach(square => {
    square.addEventListener('mousedown', () => {
        if (square.id == hitPosition) {
            result++;
            score.textContent = result;
            hitPosition = null;
        }
    })
})

function moveMole() {
    timerId = setInterval(randomSquare, 500);
}

moveMole();

function countDown() {
    currentTime--;
    timeLeft.innerHTML = currentTime;
    if (currentTime == 0){
        clearInterval(countDownTimerId);
        clearInterval(timerId);
        alert(`GAME OVER! and your final score is ${result}`)
    }
}

let countDownTimerId = setInterval(countDown, 1000);